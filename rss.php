<?php


require_once("coursefeed.php");
require_once("http.php");

$coursefeed = new CourseFeed();
$articles = $coursefeed->getArticleList(http\maybe_get_parameter($_GET, "course_id"), null);

header("Content-type: text/xml");

echo "<?xml version='1.0' encoding='UTF-8'?>
<rss version='2.0'>
<channel>
<title>Course Feed</title>
<link>http://http://coursefeed.herokuapp.com/</link>
<description>수업관련 공지를 알려줍니다</description>";
foreach ($articles as $article) {
	$title = $article["title"];
	$content = $article["content"];
	$course = $article["course_name"];
	$category = $article["category_name"];
	$id = $article["id"];

	echo "<item>
	<title>[$course] $title</title>
	<category>$category</category>
	<description>".mb_substr($article["content"], 0, 40, "utf8")."</description>
	<link>http://coursefeed.herokuapp.com/article.php?id=$id</link>
	</item>";
}
echo "</channel></rss>";

?>

