<?php

require_once("coursefeed.php");
require_once("template/course.php");
require_once("http.php");

$coursefeed = new CourseFeed();

include('base.php'); // base template

if ($_SERVER['REQUEST_METHOD'] == "GET") {
	startblock('content');
    template\course\renderInsertCourseForm();
    endblock('content');
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (http\has_parameter($_POST, "name") && http\has_parameter($_POST, "code") && http\has_parameter($_POST, "year")) {
        $name = $_POST["name"];
        $code = $_POST["code"];
        $year = $_POST["year"];
        $coursefeed->insertCourse($code, $name, $year);
    }
}

?>
