var id_checked = false;

function formValidate() {
	var $start = $("startdate");
	var $end = $("enddate");
    if (!($start !== null && $end !== null)) {
        return;
    }

	var $valid = false;
	// date validate
	if($end.value < $start.value){
		$end.setStyle({
			backgroundColor: '#ffdddd',
			border : 'solid red 1px'
		});
		$valid = false;
	} else {
		$end.setStyle({
			backgroundColor: '#ffffff',
			border : 'none'
		});
		$valid = true;
	}
	// for validate
	$valid = $valid && ($("title") === null || $("title").value !== "") &&
             ($("contentarea") === null || $("contentarea").value !== "");
    if ($$("form.article #submit")) {
        $$("form.article #submit")[0].disabled = !$valid;
    }
}

function upvoteSuccess(ajax) {
    var obj = JSON.parse(ajax.responseText);
    $$(".upvotes span.count")[0].update(obj.upvotes);
    $$(".upvotes button")[0].disabled = "disabled";
    $$(".upvotes button")[0].addClassName("pure-button-success");
    $$(".upvotes button span")[0].update("upvoted");
}

function upvote(event) {
    var element = $$(".upvotes button")[0];
    var article_id = element.readAttribute('data-article-id');
    new Ajax.Request("upvote.php", {
        parameters: {article_id: article_id},
        onSuccess: upvoteSuccess
    });
}


function checkIdRedundancy()
{
    var id = $$('#joinform input.id')[0].value;
    new Ajax.Request("id_check.php",
                     {method: "post",
                      parameters: {id: id},
                      onSuccess: passIdCheck,
                      onFailure: failIdCheck,
                      onException: failIdCheck}
                    );
}

function passIdCheck(ajax)
{
    $$('#joinform input.id')[0].removeClassName("id-check-fail");
    $$('#joinform input.id')[0].addClassName("id-check-pass");
    id_checked = true;
}

function failIdCheck(ajax)
{
    $$('#joinform input.id')[0].removeClassName("id-check-pass");
    $$('#joinform input.id')[0].addClassName("id-check-fail");
    id_checked = false;
}

function checkFillInput()
{
    if( !$$('#joinform input')[1].value.match(/^.{4,20}$/)
        || !$$('#joinform input')[2].value.match(/^.{1,40}$/)
        || !$$('#joinform input')[3].value.match(/^.{1,}[@].{1,}$/)
        || !id_checked)
        $$('#joinform input.joinButton')[0].disabled = "disabled";
    else
        $$('#joinform input.joinButton')[0].disabled = "";
}

function checkDeleteCheckbox() {
    var checked = false;
    var checkboxes = $$("#article-list input[type=checkbox]");
    if (checkboxes) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                checked = true;
                break;
            }
        }
    }
    if ($$("#article-list input[type=submit]").length > 0) {
        $$("#article-list input[type=submit]")[0].disabled = !checked;
    }
}

document.observe("dom:loaded", function() {
    if ($("startdate")) {
       $("startdate").observe("change", formValidate);
    }
    if ($("enddate")) {
        $("enddate").observe("change", formValidate);
    }

    if ($("title")) {
        $("title").observe("keyup",formValidate);
    }

    if ($("contentarea")) {
        $("contentarea").observe("keyup",formValidate);
    }

    if ($$(".upvotes button").length > 0) {
        $$(".upvotes button")[0].observe("click", upvote);
    }

    if ($$('#joinform input.id').length > 0) {
        $$('#joinform input.id')[0].observe("keyup",checkIdRedundancy);
         checkFillInput();
    }

    if( $('joinform') ){
        $('joinform').observe("change", checkFillInput);
        $('joinform').observe("keyup", checkFillInput);

        checkFillInput();
    }

    if( $('searchform') ){
        new Ajax.Autocompleter("autocomplete", "autocomplete_choices", "./course_id.php", {
            updateElement : CourseSearch.action.select,
            method: "GET",
            frequency: 0.2
        });
        CourseSearch.action.refresh();
    }

    if ($$('form.article').length > 0) {
        formValidate();
    }

    if ($("article-list")) {
        checkDeleteCheckbox();
        var checkboxes = $$("#article-list input[type=checkbox]");
        checkboxes.each(function (checkbox) {
            checkbox.observe("click", checkDeleteCheckbox);
        });
    }
});
