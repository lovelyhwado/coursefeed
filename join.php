<?php
include_once("coursefeed.php");
require_once('template/joinform.php');

include('base.php'); // base template


if($_SERVER["REQUEST_METHOD"] == "GET")
{
	startblock('content');
	template\joinform\renderJoinForm();
	endblock();
}
else if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$coursefeed = new CourseFeed();

	$id = $_POST['id'];
	$password = $_POST['password'];
	$name = $_POST['name'];
	$email = $_POST['email'];

	if($coursefeed->checkUser($_POST['id']))
	{
		//echo"<meta http-equiv='Refresh' content='0;URL=join.php'>";
	}
	else
	{
		$coursefeed->insertUser($id, $password, $name, $email);
		startblock('content');
		template\joinform\renderJoinConfirm();
		endblock();
		$coursefeed->genCertificateCode($email,$id);
		//echo"<meta http-equiv='Refresh' content='0;URL=index.php'>";
	}
}

?>
